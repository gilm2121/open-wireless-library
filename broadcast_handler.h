#pragma once

#include "Message.h"
#include "ThreadManager.h"
#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>

/************************************************************************
 * broadcast_handler													*
 * this class organizes the communication through broadcast messages	*
 * broadcast messages aren't encrypted and packet loss is possible... :(*
 ************************************************************************/

class broadcast_handler
{
private:
	std::queue<Message>*		_buffer;
	std::mutex*					_buffer_mu;
	std::condition_variable*	_cv;

public:
	broadcast_handler();
	~broadcast_handler();
	void send(Message msg);
	Message recv();
	void handle(Message msg);
};

