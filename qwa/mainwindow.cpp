#include "mainwindow.h"
#include "ui_mainwindow.h"

std::mutex              MainWindow::_mu;
std::condition_variable MainWindow::_cv_tabs_maker;
std::queue<std::string> MainWindow::_new_tabs;

void receiver(Manager& man, Device d, void* arg);
void P2PCallback(Device d, void *arg);

//constructor and destructor:
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _man = &Manager::getManager();
    _man->setAppID("first1");
    _man->setName("DrSudo7");
    _man->setP2PCallback(&P2PCallback, (void*)this);
    std::thread t(&MainWindow::tabsMaker, this);
    std::thread(&MainWindow::lookupThread, this).detach();
    t.detach();
}
MainWindow::~MainWindow()
{
    delete ui;
}

//events:
void MainWindow::on_pushButton_clicked()
{
    std::string s = ui->lineEdit->text().toStdString();
    if(s.find("create") == 0)
    {
        add_tab(s.substr(7));
    }
    else if(s.find("hide") == 0)
    {
        for(unsigned int i = 0; i < ui->tabWidget->count(); i++)
            if(ui->tabWidget->tabText(i) == QString::fromStdString(s.substr(5)))
            {
                //ui->tabWidget->widget(i)->
            }
    }
    else if(ui->tabWidget->count())
    {
        ((QTextBrowser*)ui->tabWidget->currentWidget())->append("you: " + ui->lineEdit->text());
        ((QTextBrowser*)ui->tabWidget->currentWidget())->verticalScrollBar()->setValue(((QTextBrowser*)ui->tabWidget->currentWidget())->verticalScrollBar()->maximum());
        for(std::set<Device>::iterator it = _man->_known_devs.begin(); it != _man->_known_devs.end(); it++)
        {
            if( QString::fromStdString(((Device) *it).getName()) == ui->tabWidget->tabText(ui->tabWidget->currentIndex()))
                _man->send(*it, (ui->lineEdit->text()).toStdString());
        }
    }
    ui->lineEdit->clear();
}
void MainWindow::on_tabWidget_tabCloseRequested(int index)
{
    ui->tabWidget->removeTab(index);
}

//helpers:
void MainWindow::tfunc()
{
    if(_new_tabs.empty())
        return;
    std::string name = _new_tabs.front();
    _new_tabs.pop();
    for(unsigned int i = 0; i < ui->tabWidget->count(); i++)
        if (ui->tabWidget->tabText(i).toStdString() == name)
            return;
    ui->tabWidget->addTab(new QTextBrowser, QString::fromStdString(name));
    Log::D("aaa");
}
void MainWindow::add_tab(std::string name)
{
    MainWindow::_mu.lock();
    MainWindow::_new_tabs.push(name);
    MainWindow::_mu.unlock();
    MainWindow::_cv_tabs_maker.notify_all();
}
void MainWindow::tabsMaker()
{
    std::string name;
    bool flag = true;
    while(true)
    {
        std::unique_lock<std::mutex> ul(MainWindow::_mu);
        MainWindow::_cv_tabs_maker.wait(ul, [this]{return !_new_tabs.empty();});
        QMetaObject::invokeMethod(this, "tfunc", Qt::BlockingQueuedConnection);
        ul.unlock();
    }
}

//owl:
void P2PCallback(Device d, void* arg)
{
    Log::D("p2p_callback");
    Manager &man = Manager::getManager();
    Log::D("add tab");
    std::set<Device> known_devs = man.devicesLookup(0);
    MainWindow::add_tab( ((Device)*known_devs.find(d)).getName() );
    std::thread recv(receiver, std::ref(man), d, arg);
    recv.detach();
}

void receiver(Manager& man, Device d, void* arg)
{
    MainWindow *mw = (MainWindow*)arg;
    Log::D("receiver");
    while(true)
    {
        Message msg = man.recv(d);
        QString name = QString::fromStdString(msg.getSrc().getName());
        for(int i = 0; i < mw->ui->tabWidget->count(); i++)
            if(mw->ui->tabWidget->tabText(i) == name)
            {
                ((QTextBrowser*)mw->ui->tabWidget->widget(i))->append(name + ": " + QString::fromStdString(msg.getData()));
                ((QTextBrowser*)mw->ui->tabWidget->widget(i))->verticalScrollBar()->setValue(((QTextBrowser*)mw->ui->tabWidget->widget(i))->verticalScrollBar()->maximum());
            }
    }
}

void MainWindow::lookupThread()
{
    int i;
    //while(true)
    {
        std::set<Device> devs = _man->devicesLookup(1000000);
        for(std::set<Device>::iterator it = devs.begin(); it != devs.end(); it++)
        {
            std::string name = ((Device) *it).getName();
            for(i = 0; i < ui->tabWidget->count(); i++)
            {
                if(ui->tabWidget->tabText(i) == QString::fromStdString(name))
                {
                    break;
                }
            }
            if(i == ui->tabWidget->count())
            {
                _man->connect(*it);
            }
        }
        usleep(10 * 1000000);

    }
}

void MainWindow::on_lineEdit_returnPressed()
{
    on_pushButton_clicked();
}
