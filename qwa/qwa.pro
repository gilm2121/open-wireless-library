#-------------------------------------------------
#
# Project created by QtCreator 2016-05-30T16:57:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qwa
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    AES.cpp \
    broadcast_handler.cpp \
    Device.cpp \
    Log.cpp \
    LorconContext.cpp \
    Manager.cpp \
    MD5.cpp \
    Message.cpp \
    p2p_handler.cpp \
    RSA.cpp \
    ThreadManager.cpp

HEADERS  += mainwindow.h \
    AES.h \
    broadcast_handler.h \
    Device.h \
    Log.h \
    LorconContext.h \
    Manager.h \
    MD5.h \
    Message.h \
    p2p_handler.h \
    RSA.h \
    ThreadManager.h

FORMS    += mainwindow.ui

unix: CONFIG += link_pkgconfig -std=c++11
CONFIG += c++11

LIBS += -lorcon2
