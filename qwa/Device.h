#pragma once

#include <string>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/if.h>
#include <netdb.h>
#include <stdio.h>
#include "Log.h"
#include "RSA.h"

/********************************************************
 * Device												*
 * this class saves the data about a specific device.	*
 * this data is provided by a lookup packet,			*
 * (either request or response),						*
 * from a nearby device.								*
 ********************************************************/

class Device
{
private:
	std::string	_name;		//	The name of the device
	std::string	_mac;		//	The Mac address of the device
	RSA_KEY		_rsa_e;		//	RSA public key - e
	RSA_KEY		_rsa_n;		//	RSA public key - n
	std::string	_aes_key;	//	AES key - only initiated when a connection is made with the device

	static std::string my_mac();
	static std::string my_name();

public:
	//C'tors:
	Device(std::string name, std::string mac, std::string pub_key, std::string aes);
	Device(std::string name, std::string mac, std::string pub_key);
	Device(std::string name, std::string mac);
	Device(std::string mac);
	Device(const Device &other);

	//D'tor:
	~Device();

	static Device* me();

	//getters:
	std::string			getName()const;
	std::string			getMac()const;
	std::string			getAESKey()const;
	std::string			getPublicKey()const;
	RSA_KEY				getN()const	{return _rsa_n;};
	RSA_KEY				getE()const	{return _rsa_e;};
	//setters:
	void	setPublicKey(std::string k);
	void	setAESKey(std::string k);
	void	setName(std::string name);
	//operators:
	bool	operator==(const Device &rhc)const;
	bool	operator==(const std::string &mac)const;
	bool	operator<(const Device &rhc) const;

};

