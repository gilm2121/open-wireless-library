#include "ThreadManager.h"
#define PADDING_STRING "(((("
#define PADDING_LEN 4

ThreadManager::ThreadManager(Manager *m)
{
	_man = m;
	_l_exec_q = new std::queue<Message>();
	l_exec_mu = new std::mutex();

	_send_q = new std::queue<Message>();
	_send_mu = new std::mutex();

	_cv_send = new std::condition_variable();
	_cv_exec  = new std::condition_variable();

	_context = new LorconContext(this);

	_send_buffers = new std::map<std::queue<Message>*, std::mutex*>() ;
	_send_buf_mu = new std::mutex();

	_listener_t = new std::thread(&ThreadManager::listener, this);
	_exec_t = new std::thread(&ThreadManager::executioner, this);
	_sender_t = new std::thread(&ThreadManager::sender, this);
	//_ack_sender_t = new std::thread(&ThreadManager::ack_sender, this);

	_listener_t->detach();
	_exec_t->detach();
	_sender_t->detach();
	//_ack_sender_t->detach();
}

ThreadManager::~ThreadManager()
{
	delete this->_l_exec_q;
	delete this->l_exec_mu;
	delete this->_send_q;
	delete this->_send_mu;
	delete this->_cv_send;
	delete this->_cv_exec;
	delete this->_context;
	delete this->_send_buffers;
	delete this->_send_buf_mu;
	delete this->_listener_t;
	delete this->_exec_t;
	delete this->_sender_t;
	//delete this->_ack_sender_t;
}

void ThreadManager::send(Message msg)
{
	{
		std::lock_guard<std::mutex> lk(*_send_mu);
		msg.generateSerial();
		_send_q->push(msg);
	}
	_cv_send->notify_all();
}
void ThreadManager::sender()
{

	Log::D("S");
	srand(time(NULL));
	while(true)
	{
		std::unique_lock<std::mutex> lk(*_send_mu);
		_cv_send->wait(lk, [this]{return this->_send_q->size() > 0;});
		Message msg(_send_q->front());
		_send_q->pop();
		lk.unlock();
		//send the MESSAGE
		{
			_context->send(std::string(PADDING_STRING + _man->getAppID() + msg.toString()));
		}
	}


}
void ThreadManager::executioner()
{

	Log::D("E");
	while(true)
	{
		std::unique_lock<std::mutex> lk(*l_exec_mu);
		_cv_exec->wait(lk, [this]{return this->_l_exec_q->size() > 0;});
		Message msg(_l_exec_q->front());
		_l_exec_q->pop();
		lk.unlock();
        _man->handle(msg);
	}
}
void ThreadManager::listener()
{
	Log::D("L");
	if(_man == NULL)
		Log::E("man is null");
	if(_context == NULL)
		Log::E("con is null");
	_context->sniff();
}

ThreadManager& ThreadManager::getThreadManager(Manager *m)
{
	static ThreadManager tm(m);
	return tm;
}

void ThreadManager::sandbox(Message msg)
{
	if(msg.getType() == Message::DATA && msg.getDst().getMac() ==  Message::BROADCAST)
	{
		Message msg_res("gotcha: " + msg.getData());

		{
			std::lock_guard<std::mutex> lk(*this->l_exec_mu);
			this->_l_exec_q->push(msg_res);
		}
		this->_cv_exec->notify_all();
	}
}

void ThreadManager::push_exec(Message msg)
{
	{
		std::lock_guard<std::mutex> lk(*l_exec_mu); //RIP In Pepperonis naming conventions
		this->_l_exec_q->push(msg);
	}
	this->_cv_exec->notify_all();
}

std::string ThreadManager::getAppID()
{
	return _man->getAppID();
}

std::string ThreadManager::getMac()
{
	return _man->getMac();
}

void ThreadManager::registerSendBuffer(std::queue<Message>* q , std::mutex* m)
{
	this->_send_buf_mu->lock();
	std::pair<std::queue<Message>*, std::mutex*> pa(q,m);
	_send_buffers->insert(pa);
	this->_send_buf_mu->unlock();
}

int ThreadManager::getMode()
{
	return _man->getMode();
}

void ThreadManager::ack_sender()
{

	while(true)
	{
		usleep(10000);
		_send_buf_mu->lock();
		for(std::map<std::queue<Message>*, std::mutex*>::iterator it = _send_buffers->begin(); it != _send_buffers->end(); it++)
		{
			it->second->lock();
			if(it->first->size())
				send(it->first->front());
			it->second->unlock();
		}
		_send_buf_mu->unlock();
	}

}
