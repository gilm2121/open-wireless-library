#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QtWidgets/QTextBrowser>
#include <QMetaObject>
#include <QScrollBar>

#include <iostream>
#include <thread>
#include <unistd.h>
#include <queue>
#include <mutex>
#include <condition_variable>

#include "Manager.h"
#include "Message.h"
#include "Log.h"
#include "Device.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    Ui::MainWindow* ui;

    static void add_tab(std::string name);
    void newMsg(std::string msg);
private slots:
    void on_pushButton_clicked();
    void on_tabWidget_tabCloseRequested(int index);

    void tfunc();
    void tabsMaker();

    void on_lineEdit_returnPressed();

private:
    static std::mutex               _mu;
    static std::condition_variable  _cv_tabs_maker;
    static std::queue<std::string>  _new_tabs;
    std::string                     _new_tab_name;
    void lookupThread();



    Manager*                        _man;
};

#endif // MAINWINDOW_H
