#include "Message.h"
/*
 * Message
 * A message of OWL protocol.
 * This class allows creating all types of Messages matching the protocol.
 * The class provides simple interface to create Messages and prepare them for sending.
 */
const std::string Message::BROADCAST	= "000000000000";
//constructors and destructor:
Message::Message() : Message(std::string("")){}
Message::Message(std::string data) : Message(DATA, data){}
Message::Message(unsigned char type, std::string data) : Message(Device(std::string(BROADCAST)), type, data){}
Message::Message(Device dst, unsigned char type, std::string data)
{
	Device *src = Device::me();
	_src = NULL;
	_dst = NULL;
	setSrc(*src);
	setDst(dst);
	setType(type);
	setData(data);
	setSeq(0);
	setCarry(0);
	setSerial("");
	delete src;
}

Message::Message(Device src, Device dst, unsigned char type, std::string data)
{
	_src = NULL;
	_dst = NULL;
	setSrc(src);
	setDst(dst);
	setType(type);
	setData(data);
	setSeq(0);
	setCarry(0);
	setSerial("");
}

Message::Message(const Message &m)
{
	_src = NULL;
	_dst = NULL;
	setSrc(m.getSrc());
	setDst(m.getDst());
	setType(m.getType());
	setData(m.getData());
	setLen(m.getLen());
	setSeq(m.getSeq());
	setChecksum(m.getChecksum());
	setCarry(m.getCarry());
	setSerial(m.getSerial());
}

Message::~Message()
{
	if(_src)
		delete _src;
	if(_dst)
		delete _dst;
}

//prepare the string for sending:
std::string Message::toString()
{
	std::string msg = _serial, src, dst;

	src = hex_to_string(_src->getMac());
	dst = hex_to_string(_dst->getMac());

	for(unsigned int i = 0; i < src.length(); i ++)
		msg.push_back(src[i]);
	for(unsigned int i = 0; i < dst.length(); i ++)
		msg.push_back(dst[i]);

    msg.push_back((_len >> 8) &0xFF);
    msg.push_back(_len &0xFF);

    msg.push_back((_seq >> 8) &0xFF);
    msg.push_back(_seq &0xFF);

    msg.push_back(_type);
    msg.push_back(_checksum);
    msg.push_back(_encryptionCarry);

    for(int i = 0; i < _len; i ++)
    	msg.push_back(_data[i]);

	return msg;
}

//build a Message object from a string (mostly a string captured by lorcon:
Message Message::fromString(std::string raw)
{
	std::string serial = raw.substr(0, 5);
	std::string src_mac = string_to_hex(raw.substr(5, 6));
	std::string	dst_mac = string_to_hex(raw.substr(11, 6));

	unsigned short int len = (unsigned char)raw[18] + (unsigned char)raw[17] * 16 * 16;
	unsigned short int seq = (unsigned char)raw[20] + (unsigned char)raw[19] * 16 * 16;

	unsigned char type = raw[21];
	unsigned char checksum = raw[22];
	unsigned char carry = raw[23];

	std::string data = "";
	for(unsigned int i = 24; i < raw.length(); i ++)
		data.push_back(raw[i]);

	Message m(Device(src_mac), Device(dst_mac), type, data);
	m.setSerial(serial);
	m.setSeq(seq);
	m.setChecksum(checksum);
	m.setLen(len);
	m.setCarry(carry);

	return m;
}

//encryption:
void Message::encrypt(AES a)
{
	_encryptionCarry = (16 - _data.length() % 16) % 16;
	setData(a.encrypt(_data));
}
void Message::decrypt(AES a)
{
	setData(a.decrypt(_data).substr(0, _data.length() - _encryptionCarry));
}

//getters:
Device Message::getSrc() const
{
	return *_src;
}
Device Message::getDst() const
{
	return *_dst;
}
unsigned short int Message::getLen() const
{
	return _len;
}
unsigned char Message::getChecksum() const
{
	return _checksum;
}
unsigned short int Message::getSeq() const
{
	return _seq;
}
unsigned char Message::getType() const
{
	return _type;
}
unsigned char Message::getCarry() const
{
	return _encryptionCarry;
}
std::string Message::getData() const
{
	return _data;
}
std::string Message::getSerial() const
{
	return _serial;
}

//setters:
void Message::setSrc(Device src)
{
	if(_src)
		delete _src;
	_src = new Device(src);
}
void Message::setDst(Device dst)
{
	if(_dst)
		delete _dst;
	_dst  = new Device(dst);
}
void Message::setSeq(unsigned short int seq)
{
	_seq = seq;
}
void Message::setType(unsigned char type)
{
	_type = type;
}
void Message::setLen(unsigned short int len)
{
	_len = len;
}
void Message::setChecksum(unsigned char checksum)
{
	_checksum = checksum;
}
void Message::setCarry(unsigned char carry)
{
	_encryptionCarry = carry;
}
void Message::setData(std::string data)
{
	_data = data;
	setLen(_data.length());
	setChecksum(checksum(_data));
}
void Message::setSerial(std::string serial)
{
	_serial = serial;
}
void Message::generateSerial()
{
	_serial = "";
	for(unsigned int i = 0; i < 5; i ++)
		_serial.push_back((unsigned char)rand()%256);
}

//support functions:
char Message::checksum(std::string data)
{
	int x = 0;
	for(unsigned int i = 0; i < data.length(); i++)
		x += data[i];
	x %= 255;
	x++;
	return char(x);
}
Message& Message::operator=(Message &m)
{
	setSrc(m.getSrc());
	setDst(m.getDst());
	setType(m.getType());
	setData(m.getData());
	setLen(m.getLen());
	setChecksum(m.getChecksum());
	setSeq(m.getSeq());
	return *this;
}
std::string Message::string_to_hex(const std::string& input)
{
    static const char* const lut = "0123456789abcdef";
    size_t len = input.length();
    unsigned char c;
    std::string output;

    output.reserve(2 * len);
    for (size_t i = 0; i < len; ++i)
    {
        c = input[i];
        output.push_back(lut[c >> 4]);
        output.push_back(lut[c & 15]);
    }
    return output;
}
std::string Message::hex_to_string(const std::string& input)
{
    static const char* const lut = "0123456789abcdef";
    size_t len = input.length();

    std::string output;
    output.reserve(len / 2);
    for (size_t i = 0; i < len; i += 2)
    {
        char a = input[i];
        const char* p = std::lower_bound(lut, lut + 16, a);

        char b = input[i + 1];
        const char* q = std::lower_bound(lut, lut + 16, b);

        output.push_back(((p - lut) << 4) | (q - lut));
    }
    return output;
}
