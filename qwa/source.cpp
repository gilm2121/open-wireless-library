#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include "Device.h"
#include "Message.h"
#include "Log.h"
#include "Manager.h"
#include <unistd.h>
#include "RSA.h"
#include "MD5.h"
#include "AES.h"


void receiver(Manager& man, Device d);
void P2PCallback(Device d);
void side1();
void side2();

int main()
{
	side1();
	while(true);//*/
}

void P2PCallback(Device d)
{
	Log::D("p2p_callback");
	Manager &man = Manager::getManager();
	std::thread recv(receiver, std::ref(man), d);
	recv.detach();
	std::string s = "";
	while(s != "quit")
	{
		Log::D("insert message:");
		std::getline(std::cin, s);
		man.sendFile(d,s);
		man.send(d, s);
		s = "quit";
	}
}

void receiver(Manager& man, Device d)
{
	Log::D("receiver");
	while(true)
	{
		Message msg = man.recv(d);
		std::cout << msg.getSrc().getName() << " - " << msg.getSeq() << ": " << msg.getData() << std::endl;
	}
}

void side1()
{
	Manager& man = Manager::getManager();
	man.setAppID("first1");
	man.setName("DrSudo7");
	man.setP2PCallback(P2PCallback);
	std::set<Device> devs_set = man.devicesLookup(1);
	std::vector<Device> devices;

	std::copy(devs_set.begin(), devs_set.end(), std::back_inserter(devices));
	for(unsigned int i = 0; i < devices.size(); i++)
		std::cout << i << " : " << devices[i].getName() << std::endl;
	int sel = 0;
	std::cout << "-----------------\nChoose device to connect to: ";
	std::cin >> sel;
	man.connect(devices[sel]);
	getc(stdin);
}
void side2()
{
	Manager& man = Manager::getManager();
	man.setAppID("first1");
	man.setName("gigulush4");
	man.setP2PCallback(P2PCallback);
}
