#include "Log.h"

/****************************************************************
 * Log															*
 * a log that prints comments, errors and events to the screen	*
 * the log also provides information about						*
 * the time and thread on which the event took place.			*
 ****************************************************************/

// init static values:
int Log::_i = 1;
char Log::_mod = Log::ALL;
std::mutex Log::_mu;
std::map<long, short> Log::threads;
short Log::threadIndex = 1;

Log::Log() { }

Log::~Log() { }

void Log::setModel(char mod)
{
    Log::_mod = mod;
}

void Log::E(std::string msg)
{
    if(_mod & Log::ERR)
    {
        Log::_mu.lock();
        std::cout << _i << ") " << currentDateTime() << " - T" << getThread() << " ~ " << "<ERROR> :" << msg << std::endl;
        _i++;
        Log::_mu.unlock();
    }
}

void Log::D(std::string msg)
{
    if(_mod & Log::DBG)
    {
        Log::_mu.lock();
        std::cout << _i << ") " << currentDateTime() << " - T" << getThread() << " ~ " << "<DEBUG> :" << msg << std::endl;
        _i++;
        Log::_mu.unlock();
	}
}

short Log::getThread()
{
    long thread_id = pthread_self();

    if(Log::threads.find(thread_id) == Log::threads.end()) // thread id not found:
    {
        threads[thread_id] = threadIndex++;
        return threadIndex - 1;
    }
    else // thread id found:
        return Log::threads.find(thread_id)->second;
}

const std::string Log::currentDateTime()
{
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%X", &tstruct);

    return buf;
}
