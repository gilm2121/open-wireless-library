#pragma once

#include <iostream>
#include <string>
#include <algorithm>
#include <pcap.h>
#include "Log.h"
#include "Message.h"
#include "ThreadManager.h"
#include "Manager.h"
extern "C"
{
#include<lorcon2/lorcon.h>
#include<lorcon2/lorcon_packet.h>
}

//Necessary definition of lorcon's data structures:
#define MAX_IFNAME_LEN		32
#define LORCON_WEPKEY_MAX	26
typedef struct lorcon_wep
{
	u_char bssid[6];
	u_char key[LORCON_WEPKEY_MAX];
	int len;

	struct lorcon_wep *next;
} lorcon_wep_t;
struct lorcon
{
	char drivername[32];

	char ifname[MAX_IFNAME_LEN];
	char vapname[MAX_IFNAME_LEN];

	pcap_t *pcap;

	/* Only capture_fd is assumed to be selectable */
	int inject_fd, ioctl_fd, capture_fd;

	int packets_sent;
	int packets_recv;

	int dlt;

	int channel;

	char errstr[LORCON_STATUS_MAX];

	uint8_t original_mac[6];

	int timeout_ms;

	void *auxptr;

	lorcon_handler handler_cb;
	void *handler_user;

	int (*close_cb)(lorcon_t *context);

	int (*openinject_cb)(lorcon_t *context);
	int (*openmon_cb)(lorcon_t *context);
	int (*openinjmon_cb)(lorcon_t *context);

	int (*setchan_cb)(lorcon_t *context, int chan);
	int (*getchan_cb)(lorcon_t *context);

	int (*sendpacket_cb)(lorcon_t *context, lorcon_packet_t *packet);
	int (*getpacket_cb)(lorcon_t *context, lorcon_packet_t **packet);

	int (*setdlt_cb)(lorcon_t *context, int dlt);
	int (*getdlt_cb)(lorcon_t *context);

	lorcon_wep_t *wepkeys;

	int (*getmac_cb)(lorcon_t *context, uint8_t **mac);
	int (*setmac_cb)(lorcon_t *context, int len, uint8_t *mac);
};

class ThreadManager;

/****************************************************************************************
 * LorconContext																		*
 * The porpose of this class is to wrap the lorcon struct.								*
 * while lorcon's headers provide tools to manage 802.11 communication in C language,	*
 * this class provides tools for C++.													*
 ****************************************************************************************/

class LorconContext
{
private:
	lorcon_t*	_ctx;

public:
	LorconContext(ThreadManager *tm, std::string interface = "wlan0");
	~LorconContext();
	bool send(std::string packet);
	void sniff();
	static void clb(lorcon_t *con, lorcon_packet_t *p, u_char *user);
};
