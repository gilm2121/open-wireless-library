#include "Device.h"

/********************************************************
 * Device												*
 * this class saves the data about a specific device.	*
 * this data is provided by a lookup packet,			*
 * (either request or response),						*
 * from a nearby device.								*
 ********************************************************/

//C'tors:
Device::Device(std::string name, std::string mac, std::string pub_key, std::string aes)
{
	this->_name = name;
	this->_mac = mac;
	_rsa_n = (pub_key!="")?(*(RSA_KEY *)(pub_key.c_str()+0)):0;
	_rsa_e = (pub_key!="")?(*(RSA_KEY *)(pub_key.c_str()+8)):0;
	_aes_key = aes;
}
Device::Device(std::string name, std::string mac, std::string pub_key): Device(name, mac, pub_key, ""){}
Device::Device(std::string name, std::string mac) : Device(name, mac, ""){}
Device::Device(std::string mac) : Device("unknown", mac){}
Device::Device(const Device &other) : Device(other._name, other._mac, other.getPublicKey(), other.getAESKey()){}
Device* Device::me()
{
	Device *me = new Device(Device::my_name(), Device::my_mac());
	return me;
}

//D'tors:
Device::~Device(){}

//getters:
std::string Device::getName()const
{
	return this->_name;
}
std::string Device::getMac()const
{
	return this->_mac;
}
std::string Device::getPublicKey()const
{
	std::string s(std::string((char*)&_rsa_n, sizeof(RSA_KEY)) + std::string((char*)&_rsa_e, sizeof(RSA_KEY)));
	return s;
}
std::string Device::getAESKey()const
{
	return _aes_key;
}

//setters:
void Device::setAESKey(std::string k)
{
	_aes_key = k;
}
void Device::setName(std::string name)
{
	this->_name = name;
}
void Device::setPublicKey(std::string k)
{
	_rsa_n = *(RSA_KEY *)(k.c_str()+0);
	_rsa_e = *(RSA_KEY *)(k.c_str()+sizeof(RSA_KEY));
}
//operators:
bool Device::operator==(const Device &rhc) const
{
	return this->_mac == rhc._mac;
}
bool Device::operator==(const std::string &mac)const
{
	return getMac() == mac;
}
bool Device::operator<(const Device &rhc) const
{
	return this->_mac < rhc._mac;
}

std::string Device::my_mac()
{
    static std::string my_mac_str;
    if(my_mac_str == "")
    {
        char iface[] = "wlan0";
        char *ret = new char(13);
        struct ifreq s;
        int fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);

        strcpy(s.ifr_name, iface);
        if (fd >= 0 && ret && 0 == ioctl(fd, SIOCGIFHWADDR, &s))
        {
            for (unsigned int i = 0; i < 6; ++i)
                snprintf(ret+i*2,13-i*2,"%02x",(unsigned char) s.ifr_addr.sa_data[i]);
        }
        else
        {
            delete ret;
            return "";
        }
        std::string str(ret);
        delete ret;
        my_mac_str = str;
    }
    return my_mac_str;
}
std::string Device::my_name()
{
	/*
	FILE *fp = popen( "whoami", "r" );
	char buffer[1024];
	Log::D("1x");
	fgets(buffer , 1024, fp);
	Log::D("2x");
	fclose(fp);
	return std::string(buffer);*/
	return "default-name";
}
