#pragma once

#include <vector>
#include <cstdio>
#include <string>
#include "broadcast_handler.h"
#include "p2p_handler.h"
#include "ThreadManager.h"
#include "Device.h"
#include "RSA.h"
#include "MD5.h"
#include "AES.h"
#include <set>
#include <list>
#include <map>

/********************************************************************************************************************
 * Manager																											*
 * This class supposed to make the link between the OWL library and the developers using it.						*
 * The Manager class manages the communication with other devices													*
 * and provides the developer a simple interface allowing him to send and receive packets to/from other devices,	*
 * send and listen to broadcasts according to OWL protocol,															*
 * search for nearby devices that use the same OWL supporting application,											*
 * and many more cool options! (not really)																			*
 ********************************************************************************************************************/

//delegates of callbacks to be called when new connections are made:
typedef void (*bro_clbk) ();
typedef void (*p2p_clbk) (Device dev);

class broadcast_handler;
class ThreadManager;
class p2p_handler;

class Manager
{
private:
	Device*							_me;
	char							_app_id[6];
	std::set<Device> 				_known_devs;
	bro_clbk						_bro_clbk;
	p2p_clbk						_p2p_clbk;
	ThreadManager*					_thread_manager;
	broadcast_handler*				_broadcast_handler;
	std::map<Device, p2p_handler*>	_p2p_handlers;
	int								_mode;
	RSA_KEY							_rsa_d;
	RSA_KEY						 	_rsa_e;
	RSA_KEY						 	_rsa_n;
	std::list<std::string>			_serials;

	Manager();
	Manager(Manager const&);
	void operator=(Manager const&);

	int send_raw(std::string msg_str);

public:
	//Manager is a singleton...
	static Manager& getManager();
	~Manager();

	//getters:
	bro_clbk	getBroadcastCallback();
	p2p_clbk	getP2PCallback();
	std::string getName();
	std::string getMac();
	std::string getAppID();
	RSA_KEY		getN(){return _rsa_n;};
	RSA_KEY		getE(){return _rsa_e;};
	RSA_KEY		getD(){return _rsa_d;};
	//setters:
	void setBroadcastCallback(bro_clbk clbk);
	void setP2PCallback(p2p_clbk clbk);
	bool setName(std::string name);
	void setAppID(std::string id);

	//peer to peer:
	int		send(Device device, Message msg);
	int		sendFile(Device device, std::string name);
	Message recv(Device);
	int		connect(Device);

	//broadcast:
	int		broadcast(Message);
	Message listen();

	//lookup:
	std::set<Device> devicesLookup();
	std::set<Device> devicesLookup(unsigned int milliseconds = 1);

	//handle incoming packets:
	void handle(Message msg);

	//modes:
	bool	setmode(int mode);
	int		getMode();

	//mode consts:
	static const unsigned int	NORMAL			= 0;
	static const unsigned int	REPEATER		= 1;
	static const unsigned int	OWL_REPEATER	= 2;

	static const unsigned int	WINDOW_SIZE 	= 1450;
};

