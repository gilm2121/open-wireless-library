#pragma once
#include<iostream>
#include<string>
#include<sstream>
#include<cstdlib>
#include <algorithm>
#include "Device.h"
#include "AES.h"
#include "Log.h"

/*
 * Message
 * A message of OWL protocol.
 * This class allows creating all types of Messages matching the protocol.
 * The class provides simple interface to create Messages and prepare them for sending.
 */

#define HEADER_LEN 18

class Message
{
private:
	Device*				_src;
	Device*				_dst;
	unsigned short int	_len;
	unsigned short int	_seq;
	unsigned char		_checksum;
	unsigned char		_type;
	unsigned char		_encryptionCarry;
	std::string			_data;
	std::string			_serial;

	static std::string	string_to_hex(const std::string& input);
	static std::string	hex_to_string(const std::string& input);
public:
	//constructors and destructor:
	Message();
	Message(std::string data);
	Message(unsigned char type, std::string data);
	Message(Device dst, unsigned char type, std::string data);
	Message(Device src, Device dst, unsigned char type, std::string data);
	Message(const Message &m);
	~Message();

	//to string:
	std::string toString();

	//from string:
	static Message fromString(std::string raw);

	//encryption:
	void encrypt(AES a);
	void decrypt(AES a);

	//getters:
	Device						getSrc()		const;
	Device						getDst()		const;
	unsigned short int			getSeq()		const;
	unsigned short int			getLen()		const;
	unsigned char				getType()		const;
	unsigned char				getChecksum()	const;
	unsigned char				getCarry()		const;
	std::string 				getData()		const;
	std::string 				getSerial()		const;

	//setters:
	void setSrc(Device src);
	void setDst(Device dst);
	void setSeq(unsigned short int seq);
	void setLen(unsigned short int len);
	void setType(unsigned char type);
	void setChecksum(unsigned char checksum);
	void setCarry(unsigned char carry);
	void setData(std::string data);
	void setSerial(std::string serial);
	void generateSerial();

	Message& operator=(Message &m);

	static char 		checksum(std::string data);

	//message types:
	static const unsigned char	SYN			= 128;
	static const unsigned char	ACK			= 64;
	static const unsigned char	DATA		= 32;
	static const unsigned char	LOOKUP		= 16;
	static const unsigned char	PARTIAL		= 8;
	static const unsigned char	ERR2		= 4;
	static const unsigned char	ERR1		= 2;
	static const unsigned char	FIN			= 1;
	static const std::string	BROADCAST;

};

